// used for the github page of a single PR page
// will parse the page and put the message right after "wants to merge X commits into ... from ..."
doc = $(document)

parsePrPage(doc, function(userMessage)
{
    debug_log('parsePrPage got userMessage: '+userMessage)
    var target_div = doc.find('.TableObject-item.TableObject-item--primary');
    target_div.append(userMessage);
});