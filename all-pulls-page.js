// used for the github page showing all PR's
// will get the single page of each PR, parse it and put the message right after "opened X days ago by ..."
$(document).find('.float-left.col-9.p-2.lh-condensed').each(function(index, elem) {
    pr_link=$(elem).find('.link-gray-dark.no-underline.h4.js-navigation-open').first().attr('href')

    if (pr_link === undefined) {
        console.log("Can't load PR link")
        return;
    }

    // get the PR's page
    var x = new XMLHttpRequest();
    x.open('GET', pr_link);

    x.onload = function() {
        // Parse and process the response
        var response = x.response;

        if (response.length == 0) {
            console.log("Can't load PR page")
            return;
        }

        parser = new DOMParser()
        dom = parser.parseFromString(response, "text/html");

        parsePrPage($(dom), function(userMessage)
        {
            debug_log('parsePrPage got userMessage: '+userMessage)

            $(elem).find('.opened-by').first().append(userMessage);
        });
    }

    x.onerror = function() {
        console.log("Network error");
    };
    
    x.send();
});