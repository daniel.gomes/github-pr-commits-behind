This Chrome extensions shows a message in PR-related pages with how many commits that PR's branch is behind `master`.

Single PR page:

![single pr](single-pr.png)

All PR's page:

![all prs](all-prs.png)


Installation
---------

Note: only works in Chrome.

1. Clone project.
2. Go to chrome://extensions/
3. Enable "Developer Mode"
4. Click "Load unpacked extension..."
5. Navigate to the cloned repository's root.

Problems
----------

- Sometimes Github changes the CSS structure and this extension stops working. Having said that, this used to happen a lot more than nowadays, and it has been working fine for a while.
- When navigating between pages the user message isn't always shown until a manual reload is done.