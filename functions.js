debug=false

/**
 * Debug log
 *
 * Console logs a message if debug is true.
 *
 * @param {string}
 */
function debug_log(msg)
{
    if (debug)
        console.log(msg)
}

/**
 * Parse PR page
 *
 * Parse a single PR's page and calls a callback with a message to show the user with the number of commits behind that PR is.
 *
 * @param {document} the page's document to check.
 * @param {callback} should take the user message as a parameter.
 */
function parsePrPage(doc, callback)
{
    // get PR branch's to and from branch names
    var into_branch_name = doc.find('.commit-ref.css-truncate.user-select-contain.expandable.base-ref').eq(0).find('.css-truncate-target').html()

    // there's no way to determine in Github how many commits a branch is behind other non-master branch, so if we're
    // not merging into master we return immediately
    if (into_branch_name != "master") {
        debug_log('ignoring into_branch_name '+into_branch_name)
        return
    }

    var from_branch_name = doc.find('.commit-ref.css-truncate.user-select-contain.expandable.head-ref').eq(0).find('.css-truncate-target').html()

    // get the branch's page
    var x = new XMLHttpRequest();
    current_path = window.location.pathname

    // we determine the relative link to it based on the window location (since we don't know the organization/repository
    // being looked at)
    branch_path = current_path.replace(/^\/([^\/]+)\/([^\/]+)\/(.+)/gi, "/$1/$2")
    branch_path += '/tree/' + from_branch_name

    x.open('GET', branch_path);

    x.onload = function() {
        // Parse and process the response
        var response = x.response;

        if (response.length == 0) {
            console.log("Can't load branch page")
            return;
        }

        parser = new DOMParser()
        dom = parser.parseFromString(response, "text/html");

        // get the number of commits that this branch is behind master
        var info = $(dom).find('.branch-infobar').first();

        var info_text = info.clone().children().remove().end().text();
        debug_log('info_text: '+info_text)

        var re = /(\d+) commits? behind/;
        var m = info_text.match(re);

        if (m != null) {
            behind=m[1]

            userMessage = behind == 1 ?
                "("+behind+" commit <span style='color:red;'>behind</span> master)" :
                "("+behind+" commits <span style='color:red;'>behind</span> master)";

        } else {
            // if we didn't find a "NUM commits behind" text, we check to see if it's because the branch is only ahead
            // of master
            var re = /This branch is (\d+) commits? ahead of master\./;
            var m = info_text.match(re);

            if (m == null) {
                // this means something failed
                userMessage="<span style='color:red;'>(problem obtaining commits behind master)</span>"
            }
            else {
                userMessage="(<span style='color:green;'>up-to-date</span> with master)"
            }
        }

        // call the callback with the obtained user message
        callback(userMessage)
    }

    x.onerror = function() {
        console.log("Network error");
    };

    x.send();
}